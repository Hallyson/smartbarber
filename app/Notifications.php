<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{

    public $table = 'notification_tbl';
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'booking_id', 'user_id', 'sender_id', 'title', 'sub_title',
    ];
    public function getOwnerAttribute()
    {
        return Branch::find($this->attributes['sender_id'], ['id', 'name', 'image']);
    }
    public function getUserAttribute()
    {
        return AppUsers::find($this->attributes['sender_id'], ['id', 'name', 'image']);
    }

    public  function time_elapsed_string($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'ano',
            30 * 24 * 60 * 60  =>  'mês',
            24 * 60 * 60  =>  'dia',
            60 * 60  =>  'hora',
            60  =>  'minuto',
            1  =>  'segundo'
        );
        $a_plural = array( 'year'   => 'anos',
            'month'  => 'meses',
            'day'    => 'dias',
            'hour'   => 'horas',
            'minute' => 'minutos',
            'second' => 'segundos'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' atrás';
            }
        }
    }
}
