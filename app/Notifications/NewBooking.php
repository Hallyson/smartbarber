<?php

namespace App\Notifications;

use App\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewBooking extends Notification implements ShouldQueue
{
    use Queueable;
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;
    public $message;
    public $usuario;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($username, User $user)
    {
        $this->usuario = $user;
        $this->username = $username;
        $this->message = " realizou um agendamento.";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => $this->message,
            'user' => auth()->user()
        ]);
    }

    public function toDatabase()
    {
        return [
            'message' => $this->message,
            'user' => auth()->user()
        ];
    }

//    /**
//     * Get the channels the event should broadcast on.
//     *
//     * @return Channel|array
//     */
    public function broadcastOn()
    {
        return new PrivateChannel('App.User.'.$this->usuario->id);
//        return new PrivateChannel('App.User.1');
    }
}
