/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// window._ = require('loadash');
try {
    window.$ = window.jQuery = require('jquery');

} catch (e) {
    console.log(e);
}
import Notifications from './components/Notifications.vue';
import Swal from 'sweetalert2'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('notification', Notifications);
const app = new Vue({
    el: '#app',
    data: {
        notifications: ''
    },
    created() {
        if (window.Laravel.userId) {
            axios.post(window.Laravel.baseUrl + '/user/notification').then(response => {
                this.notifications = response.data;
                console.log(response.data);
            });
            console.log(`App.User.${window.Laravel.userId}`);
            window.Echo.private(`App.User.${window.Laravel.userId}`).notification((response) => {
                // data = { "data": response };
                console.log('channel:',`App.User.${window.Laravel.userId}`);
                this.notifications.push({ "data": response });
                Swal.fire({
                    title: 'Uhul!',
                    text: 'Você tem uma nova notificação!',
                    icon: 'info',
                    confirmButtonText: 'Cool'
                })
                const audio = new Audio(window.Laravel.baseUrl + '/audio/notification.mp3');
                audio.play();
                console.log(response);
            })
        }
    }
});
