'use strict';
$(document).ready(function () {

    try {
        $('#summernote').summernote();

        $('.js-example-basic').select2();
    } catch (error) {

    }

    var table = $('#dataTable').DataTable({
        retrieve: true,
        dom: 'Bfrtip',
        language: {
            paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
            },
            sSearch: "Pesquisar",
            sEmptyTable: "Nenhum registro encontrado",
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
            sInfoFiltered: "(Filtrados de _MAX_ registros)",
            sInfoPostFix: "",
            sInfoThousands: ".",
            sLengthMenu: "_MENU_ resultados por página",
            sLoadingRecords: "Carregando...",
            sProcessing: "Processando...",
            sZeroRecords: "Nenhum registro encontrado",
        },
        buttons: [{
                extend: 'copyHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'excelHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'csvHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'pdfHtml5',
                title: new Date().toISOString()
            },
        ]
    });

    //table.destroy();

    $("#users").select2();
    $("#providers").select2();

    $("#usersSelectAll").click(function () {
        $("#users > option").prop("selected", "selected");
        $("#users").trigger("change");
    });
    $("#usersSelectDeAll").click(function () {
        $("#users > option").prop("selected", '');
        $("#users").trigger("change");

    });

});

function printDiv(divName) {

    Popup($('<div />').append($(divName).clone()).html());

    document.body.innerHTML = originalContents;
}

function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>my div</title>');
    mywindow.document.write('<link type="text/css" href="{{ asset(argon) }}/css/invoice.css" rel="stylesheet" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    setTimeout(() => {
        mywindow.print();

    }, 2000);

}

function time_ago() {

    var time = $(".timestamp").data('value');
    console.log(time);

    switch (typeof time) {
        case 'number':
            break;
        case 'string':
            time = +new Date(time);
            break;
        case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
        default:
            time = +new Date();
    }
    var time_formats = [
        [60, 'segundos', 1], // 60
        [120, '1 minuto atrás', '1 minute from now'], // 60*2
        [3600, 'munitos', 60], // 60*60, 60
        [7200, '1 hora atrás', '1 hour from now'], // 60*60*2
        [86400, 'hours', 3600], // 60*60*24, 60*60
        [172800, 'Ontem', 'Tomorrow'], // 60*60*24*2
        [604800, 'dias', 86400], // 60*60*24*7, 60*60*24
        [1209600, 'Semana passada', 'Next week'], // 60*60*24*7*4*2
        [2419200, 'semanas', 604800], // 60*60*24*7*4, 60*60*24*7
        [4838400, 'Mês passado', 'Next month'], // 60*60*24*7*4*2
        [29030400, 'meses', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
        [58060800, 'Ano passado', 'Next year'], // 60*60*24*7*4*12*2
        [2903040000, 'anos', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        [5806080000, 'Século passado', 'Next century'], // 60*60*24*7*4*12*100*2
        [58060800000, 'séculos', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
        token = 'atrás',
        list_choice = 1;

    if (seconds == 0) {
        return 'Agora'
    }
    if (seconds < 0) {
        seconds = Math.abs(seconds);
        token = 'from now';
        list_choice = 2;
    }
    var i = 0,
        format;
    while (format = time_formats[i++])
        if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
        }
    return time;
}

function copyToClipboard(id) {
    var $body = document.getElementsByTagName('body')[0];
    var secretInfo = document.getElementById(id).innerHTML;
    var $tempInput = document.createElement('INPUT');
    $body.appendChild($tempInput);
    $tempInput.setAttribute('value', secretInfo)
    $tempInput.select();
    document.execCommand('copy');
    $body.removeChild($tempInput);
    alert('tag copy.')
}

function expireChange() {
    $("#expiry_date").toggle();
    $('#max_usage').toggleClass("d-none");
}

function toggleInput(name) {
    var x = document.getElementsByName(name);
    var x = x[0];
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
